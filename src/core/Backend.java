package core;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Document;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Backend {

    public static final String xmlFile = "src/core/backendXML.xml";
    File fXmlFile;
    Document doc;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;

    public Backend() throws ParserConfigurationException {
        try {
            fXmlFile = new File(xmlFile);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(fXmlFile);
            doc.getDocumentElement().normalize();
        } catch (IOException | SAXException ex) {
            Logger.getLogger(Backend.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Getting Request List From XML
    public List<String> getListofRequests() throws ParserConfigurationException, IOException {

        List<String> listArray = new ArrayList<>();
        NodeList nList = doc.getElementsByTagName("request");

        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            Element eElement = (Element) nNode;
            String ele = (i + 1) + "-" + eElement.getElementsByTagName("name").item(0).getTextContent();
            listArray.add(ele);
        }
        return listArray;
    }

    public String getUrlByRequestIndex(int index) {
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        return eUrl.getElementsByTagName("url").item(0).getTextContent();
    }

    public String getNameByRequestIndex(int index) {
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        return eUrl.getElementsByTagName("name").item(0).getTextContent();
    }

    public String getMethodTypeByRequestIndex(int index) {
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        return eUrl.getElementsByTagName("method").item(0).getTextContent();
    }

    public String getHeadersByRequestIndex(int index) {
        String headersText = "";
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        Node nodeHeaders = eUrl.getElementsByTagName("headers").item(0);
        Element eHeaders = (Element) nodeHeaders;
        if (nodeHeaders == null) {
            return "";
        }
        int length = eHeaders.getElementsByTagName("header").getLength();
        NodeList headerList = eHeaders.getElementsByTagName("header");
        for (int i = 0; i < length; i++) {
            Element eHeader = (Element) headerList.item(i);
            String queryString = eHeader.getElementsByTagName("query").item(0).getTextContent();
            String valueString = eHeader.getElementsByTagName("value").item(0).getTextContent();
            headersText = headersText + queryString + ":" + valueString + "\n";
        }
        return headersText;
    }

    public String getFormdataByRequestIndex(int index) {
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        if (eUrl.getElementsByTagName("formdata").item(0) == null) {
            return "";
        }
        return eUrl.getElementsByTagName("formdata").item(0).getTextContent();
    }

    public String getParametersByRequestIndex(int index) {
        String paramsText = "";
        Node nodeUrl = doc.getElementsByTagName("request").item(index);
        Element eUrl = (Element) nodeUrl;
        Node nodeParameters = eUrl.getElementsByTagName("parameters").item(0);
        Element eParameters = (Element) nodeParameters;
        if (nodeParameters == null) {
            return "";
        }
        int length = eParameters.getElementsByTagName("field").getLength();
        NodeList parameterList = eParameters.getElementsByTagName("field");
        for (int i = 0; i < length; i++) {
            Element eHeader = (Element) parameterList.item(i);
            String queryString = eHeader.getElementsByTagName("query").item(0).getTextContent();
            String valueString = eHeader.getElementsByTagName("value").item(0).getTextContent();
            paramsText = paramsText + queryString + ":" + valueString + "\n";
        }
        return paramsText;
    }

    // Convert List Name to ID(index)
    private int getIdfromName(String idName) {
        String[] splitArray = idName.split("-");
        return (Integer.parseInt(splitArray[0]) - 1);
    }

    public void editRequest(int index, String name, String url, String method, String headers, String parameters, String formData) throws TransformerException {
        Node node = doc.getElementsByTagName("root").item(0);
        Element rootElement = (Element) node;
        Node requestNode = rootElement.getElementsByTagName("request").item(index);
        Element requestElement = (Element) requestNode;
        //removing elements
        requestNode.removeChild(requestElement.getElementsByTagName("name").item(0));
        requestNode.removeChild(requestElement.getElementsByTagName("url").item(0));
        requestNode.removeChild(requestElement.getElementsByTagName("method").item(0));
        if (requestElement.getElementsByTagName("headers").item(0) != null ) {
        requestNode.removeChild(requestElement.getElementsByTagName("headers").item(0));
        }
         if (requestElement.getElementsByTagName("parameters").item(0) != null ) {
        requestNode.removeChild(requestElement.getElementsByTagName("parameters").item(0));
         }
        if (requestElement.getElementsByTagName("formdata").item(0) != null ) {
        requestNode.removeChild(requestElement.getElementsByTagName("formdata").item(0));
         }

        //adding elements
        Element elementTitle = doc.createElement("name");
        elementTitle.setTextContent(name);
        requestNode.appendChild(elementTitle);

        Element elementUrl = doc.createElement("url");
        elementUrl.setTextContent(url);
        requestNode.appendChild(elementUrl);

        Element elementMethod = doc.createElement("method");
        elementMethod.setTextContent(method);
        requestNode.appendChild(elementMethod);

        if (!headers.equals("")) {
            Element elementHeaders = doc.createElement("headers");
            // Getting per row from headers textarea
            String lines[] = headers.split("\\r?\\n");
            for (String line : lines) {
                Element elementHeader = doc.createElement("header");
                elementHeaders.appendChild(elementHeader);
                String[] queryValue = line.split(":");
                Element elementHeaderQuery = doc.createElement("query");
                elementHeaderQuery.setTextContent(queryValue[0]);
                elementHeader.appendChild(elementHeaderQuery);
                Element elementHeaderValue = doc.createElement("value");
                elementHeaderValue.setTextContent(queryValue[1]);
                elementHeader.appendChild(elementHeaderValue);
            }
            requestNode.appendChild(elementHeaders);
        }

        if (!parameters.equals("")) {
            Element elementParameters = doc.createElement("parameters");
            // Getting per row from parameters textarea
            String paramLines[] = parameters.split("\\r?\\n");

            for (String paramLine : paramLines) {
                Element elementField = doc.createElement("field");
                elementParameters.appendChild(elementField);
                String[] queryValue = paramLine.split(":");
                Element elementParamQuery = doc.createElement("query");
                elementParamQuery.setTextContent(queryValue[0]);
                elementField.appendChild(elementParamQuery);
                Element elementParamValue = doc.createElement("value");
                elementParamValue.setTextContent(queryValue[1]);
                elementField.appendChild(elementParamValue);
            }
            requestNode.appendChild(elementParameters);
        }
        if (!formData.equals("")) {
            Element elementFormdata = doc.createElement("formdata");
            elementFormdata.setTextContent(formData);
            requestNode.appendChild(elementFormdata);
        }

        saveXmlFile(doc, xmlFile);

    }

    public void addRequest(String name, String url, String method, String headers, String parameters, String formData) throws TransformerException {

        Node node = doc.getElementsByTagName("root").item(0);

        Element newElement = doc.createElement("request");

        Element elementTitle = doc.createElement("name");
        elementTitle.setTextContent(name);
        newElement.appendChild(elementTitle);

        Element elementUrl = doc.createElement("url");
        elementUrl.setTextContent(url);
        newElement.appendChild(elementUrl);

        Element elementMethod = doc.createElement("method");
        elementMethod.setTextContent(method);
        newElement.appendChild(elementMethod);
        if (!headers.equals("")) {
            Element elementHeaders = doc.createElement("headers");
            // Getting per row from headers textarea
            String lines[] = headers.split("\\r?\\n");
            for (String line : lines) {
                Element elementHeader = doc.createElement("header");
                elementHeaders.appendChild(elementHeader);
                String[] queryValue = line.split(":");
                Element elementHeaderQuery = doc.createElement("query");
                elementHeaderQuery.setTextContent(queryValue[0]);
                elementHeader.appendChild(elementHeaderQuery);
                Element elementHeaderValue = doc.createElement("value");
                elementHeaderValue.setTextContent(queryValue[1]);
                elementHeader.appendChild(elementHeaderValue);
            }
            newElement.appendChild(elementHeaders);
        }
        if (!parameters.equals("")) {
            Element elementParameters = doc.createElement("parameters");
            // Getting per row from parameters textarea
            String paramLines[] = parameters.split("\\r?\\n");

            for (String paramLine : paramLines) {
                Element elementField = doc.createElement("field");
                elementParameters.appendChild(elementField);
                String[] queryValue = paramLine.split(":");
                Element elementParamQuery = doc.createElement("query");
                elementParamQuery.setTextContent(queryValue[0]);
                elementField.appendChild(elementParamQuery);
                Element elementParamValue = doc.createElement("value");
                elementParamValue.setTextContent(queryValue[1]);
                elementField.appendChild(elementParamValue);
            }
            newElement.appendChild(elementParameters);
        }
        if (!formData.equals("")) {
            Element elementFormdata = doc.createElement("formdata");
            elementFormdata.setTextContent(formData);
            newElement.appendChild(elementFormdata);
        }

        node.appendChild(newElement);

        saveXmlFile(doc, xmlFile);

    }

    //Delete Selected Test Case from xml file
    public void deleteTest(String selected) throws TransformerConfigurationException, TransformerException {
        int index = getIdfromName(selected);
        Node node = doc.getElementsByTagName("request").item(index);
        node.getParentNode().removeChild(node);
        saveXmlFile(doc, xmlFile);
    }

    //Run BackendTest
    public String runRequestTest(String idName) throws IOException {
        String returnLog = "";
        int queue = getIdfromName(idName);
        Node node = doc.getElementsByTagName("request").item(queue);
        Element element = (Element) node;
        String method = element.getElementsByTagName("method").item(0).getTextContent();
        switch (method) {
            case "GET":
                returnLog = sendGetRequest(element);
                break;
            case "POST":
                returnLog = sendPostRequest(element);
                break;
            case "PUT":
                returnLog = sendPutRequest(element);
                break;
            case "DELETE":
                returnLog = sendDeleteRequest(element);
                break;
            default:
                return "Request Method Type Not Found";
        }
        return returnLog;
    }

    // Send GET Request Function
    private String sendGetRequest(Element element) throws MalformedURLException, IOException {
        String url = element.getElementsByTagName("url").item(0).getTextContent();
        StringBuffer response = new StringBuffer();
        //Getting Parameters
        NodeList params = element.getElementsByTagName("field");
        int paramLength = params.getLength();
        //Setting Parameters

        if (paramLength != 0) {
            url = url + "?";
        }
        for (int i = 0; i < paramLength; i++) {
            Element paramElement = (Element) params.item(i);
            url = url + paramElement.getElementsByTagName("query").item(0).getTextContent() + "=" + paramElement.getElementsByTagName("value").item(0).getTextContent()+"&";
        }
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        //Getting Headers
        NodeList headers = element.getElementsByTagName("header");
        int headerLength = headers.getLength();
        //Setting Headers
        for (int i = 0; i < headerLength; i++) {
            Element headerElement = (Element) headers.item(i);
            con.setRequestProperty(
                    headerElement.getElementsByTagName("query").item(0).getTextContent(),
                    headerElement.getElementsByTagName("value").item(0).getTextContent()
            );
        }
        int responseCode = con.getResponseCode();
        response.append("\n------- TEST START --------");
        //Show Results
        response.append("\nSending 'GET' request to URL : " + url);
        response.append("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        response.append("\n------- TEST END --------");
        //print result
        return response.toString();
    }

    // Send POST Request Function
    private String sendPostRequest(Element element) throws MalformedURLException, IOException {
        String url = element.getElementsByTagName("url").item(0).getTextContent();
        StringBuffer response = new StringBuffer();
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");

        //Getting Headers
        NodeList headers = element.getElementsByTagName("header");
        int headerLength = headers.getLength();
        //Setting Headers
        for (int i = 0; i < headerLength; i++) {
            Element headerElement = (Element) headers.item(i);
            con.setRequestProperty(
                    headerElement.getElementsByTagName("query").item(0).getTextContent(),
                    headerElement.getElementsByTagName("value").item(0).getTextContent()
            );
        }

        //Getting Parameters
        String urlParameters = "";
        NodeList params = element.getElementsByTagName("field");
        int paramLength = params.getLength();
        //Setting Parameters
        for (int i = 0; i < paramLength; i++) {
            Element paramElement = (Element) params.item(i);
            urlParameters = urlParameters + paramElement.getElementsByTagName("query").item(0).getTextContent() + "=" + paramElement.getElementsByTagName("value").item(0).getTextContent();
            urlParameters = urlParameters + "&";
        }

        //Setting Form Data
        con.setDoOutput(true);
        byte[] outputBytes = element.getElementsByTagName("formdata").item(0).getTextContent().getBytes("UTF-8");
        OutputStream os = con.getOutputStream();
        os.write(outputBytes);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        //Show Results
        response.append("\n------- TEST START --------");
        response.append("\nSending 'GET' request to URL : " + url);
        response.append("\nPost Parameters : " + urlParameters);
        response.append("\nResponse Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        response.append("\n------- TEST END --------");
        //print result
        return response.toString();

    }

    private String sendPutRequest(Element element) throws MalformedURLException, IOException {
        String url = element.getElementsByTagName("url").item(0).getTextContent();
        StringBuffer response = new StringBuffer();
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("PUT");

        //Getting Headers
        NodeList headers = element.getElementsByTagName("header");
        int headerLength = headers.getLength();
        //Setting Headers
        for (int i = 0; i < headerLength; i++) {
            Element headerElement = (Element) headers.item(i);
            con.setRequestProperty(
                    headerElement.getElementsByTagName("query").item(0).getTextContent(),
                    headerElement.getElementsByTagName("value").item(0).getTextContent()
            );
        }

        //Getting Parameters
        String urlParameters = "";
        NodeList params = element.getElementsByTagName("field");
        int paramLength = params.getLength();
        //Setting Parameters
        for (int i = 0; i < paramLength; i++) {
            Element paramElement = (Element) params.item(i);
            urlParameters = urlParameters + paramElement.getElementsByTagName("query").item(0).getTextContent() + "=" + paramElement.getElementsByTagName("value").item(0).getTextContent();
            urlParameters = urlParameters + "&";
        }

        //Setting Form Data
        con.setDoOutput(true);
        byte[] outputBytes = element.getElementsByTagName("formdata").item(0).getTextContent().getBytes("UTF-8");
        OutputStream os = con.getOutputStream();
        os.write(outputBytes);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        //Show Results
        response.append("\n------- TEST START --------");
        response.append("\nSending 'GET' request to URL : " + url);
        response.append("\nPost Parameters : " + urlParameters);
        response.append("\nResponse Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        response.append("\n------- TEST END --------");
        //print result
        return response.toString();

    }

    private String sendDeleteRequest(Element element) throws MalformedURLException, IOException {
        String url = element.getElementsByTagName("url").item(0).getTextContent();
        StringBuffer response = new StringBuffer();
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("DELETE");

        //Getting Headers
        NodeList headers = element.getElementsByTagName("header");
        int headerLength = headers.getLength();
        //Setting Headers
        for (int i = 0; i < headerLength; i++) {
            Element headerElement = (Element) headers.item(i);
            con.setRequestProperty(
                    headerElement.getElementsByTagName("query").item(0).getTextContent(),
                    headerElement.getElementsByTagName("value").item(0).getTextContent()
            );
        }

        //Getting Parameters
        String urlParameters = "";
        NodeList params = element.getElementsByTagName("field");
        int paramLength = params.getLength();
        //Setting Parameters
        for (int i = 0; i < paramLength; i++) {
            Element paramElement = (Element) params.item(i);
            urlParameters = urlParameters + paramElement.getElementsByTagName("query").item(0).getTextContent() + "=" + paramElement.getElementsByTagName("value").item(0).getTextContent();
            urlParameters = urlParameters + "&";
        }

        //Setting Form Data
        con.setDoOutput(true);
        byte[] outputBytes = element.getElementsByTagName("formdata").item(0).getTextContent().getBytes("UTF-8");
        OutputStream os = con.getOutputStream();
        os.write(outputBytes);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();

        //Show Results
        response.append("\n------- TEST START --------");
        response.append("\nSending 'GET' request to URL : " + url);
        response.append("\nPost Parameters : " + urlParameters);
        response.append("\nResponse Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        response.append("\n------- TEST END --------");
        //print result
        return response.toString();

    }

    private void saveXmlFile(Document doc, String xmlFile) throws TransformerConfigurationException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(xmlFile));
        doc.getDocumentElement().normalize();
        transformer.transform(source, result);
    }
}
