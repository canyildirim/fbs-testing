package core;

import static core.Stress.xmlFile;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Core {

    public static final String xmlFile = "src/core/seleniumXML.xml";
    File fXmlFile;
    Document doc;
    DocumentBuilderFactory dbFactory;
    DocumentBuilder dBuilder;
    String tailFileName;

    public Core() throws ParserConfigurationException, SAXException, IOException {
        fXmlFile = new File(xmlFile);
        dbFactory = DocumentBuilderFactory.newInstance();
        dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
    }

    public String runSingleSeleniumTest(int index) throws IOException {
        String filePath = getTestFilePathByIndex(index);
        String[] splitString = filePath.split("test-files/");
        String fileNameJava = splitString[1];
        String[] pureFilename = fileNameJava.split("\\.");
        String lastFileName = pureFilename[0];
        String buildSyntax = "javac -cp \"test-files/selenium-java-2.38.0.jar:test-files/libs/junit-dep-4.11.jar:test-files/libs/*\" test-files/Deneme.java";
        System.out.println(buildSyntax);
        String buildLog = executeCommand(buildSyntax);
        System.out.println("==" + buildLog);
        String executeSyntax = " cd test-files/ && java -cp \".:selenium-java-2.38.0.jar:libs/junit-dep-4.11.jar:libs/*\" org.junit.runner.JUnitCore " + lastFileName;
        System.out.println(executeSyntax);
        String executeLog = executeCommand(executeSyntax);
        System.out.println(executeLog);
        return executeLog;
    }

    // Runs all selenium tests on list model
    public void runAllSeleniumTests(DefaultListModel model) throws IOException {
        for (int it = 0; it < model.getSize(); it++) {
            runSingleSeleniumTest(it);
        }
    }

    public void executeSeleniumIDE() throws IOException {
        String command = "/Applications/Firefox.app/Contents/MacOS/firefox --chrome 'chrome://selenium-ide/content/' ";
        // /Applications/Firefox.app/Contents/MacOS/firefox --chrome 'chrome://selenium-ide/content/'
        String output = executeCommand(command);
        System.out.println(output);
    }

    private String executeCommand(String command) {

        StringBuffer output = new StringBuffer();
        String[] cmd = {"/bin/sh", "-c", command};

        Process p;
        try {
            p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }

    public void addStressTestToXml(String name, String filePath) throws TransformerException {
        Node rootNode = doc.getElementsByTagName("root").item(0);
        Element eTest = doc.createElement("test");
        Element eName = doc.createElement("name");
        eName.setTextContent(name);
        Element ePath = doc.createElement("path");
        ePath.setTextContent(filePath);
        eTest.appendChild(eName);
        eTest.appendChild(ePath);
        rootNode.appendChild(eTest);
        saveXmlFile(doc, xmlFile);
    }

    public List<String> getListofRequests() throws ParserConfigurationException, IOException {
        List<String> listArray = new ArrayList<>();
        NodeList nList = doc.getElementsByTagName("test");

        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            Element eElement = (Element) nNode;
            String ele = (i + 1) + "-" + eElement.getElementsByTagName("name").item(0).getTextContent();
            listArray.add(ele);
        }
        return listArray;
    }

    private void saveXmlFile(Document doc, String xmlFile) throws TransformerConfigurationException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File(xmlFile));
        doc.getDocumentElement().normalize();
        transformer.transform(source, result);
    }

    public void deleteTest(int index) throws TransformerConfigurationException, TransformerException {
        Node node = doc.getElementsByTagName("test").item(index);
        node.getParentNode().removeChild(node);
        saveXmlFile(doc, xmlFile);
    }

    public String getTestNameByIndex(int index) {
        Node rootNode = doc.getElementsByTagName("root").item(0);
        Element rootElement = (Element) rootNode;
        Node testNode = rootElement.getElementsByTagName("test").item(index);
        Element testElement = (Element) testNode;
        return testElement.getElementsByTagName("name").item(0).getTextContent();
    }

    public String getTestFilePathByIndex(int index) {
        Node rootNode = doc.getElementsByTagName("root").item(0);
        Element rootElement = (Element) rootNode;
        Node testNode = rootElement.getElementsByTagName("test").item(index);
        Element testElement = (Element) testNode;
        return testElement.getElementsByTagName("path").item(0).getTextContent();
    }

    public void editStressTest(int index, String name, String filePath) throws TransformerException {
        Node node = doc.getElementsByTagName("root").item(0);
        Element rootElement = (Element) node;
        Node requestNode = rootElement.getElementsByTagName("test").item(index);
        Element requestElement = (Element) requestNode;

        requestNode.removeChild(requestElement.getElementsByTagName("name").item(0));
        requestNode.removeChild(requestElement.getElementsByTagName("path").item(0));

        Node nameNode = doc.createElement("name");
        nameNode.setTextContent(name);
        requestNode.appendChild(nameNode);

        Node pathNode = doc.createElement("path");
        pathNode.setTextContent(filePath);
        requestNode.appendChild(pathNode);

        saveXmlFile(doc, xmlFile);
    }
}
