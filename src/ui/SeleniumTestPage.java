/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import core.Core;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.xml.sax.SAXException;

/**
 *
 * @author cyildirim
 */
public class SeleniumTestPage extends javax.swing.JFrame {

    Core fbsCore;

    /**
     * Creates new form SeleniumTestPage
     */
    public SeleniumTestPage() throws ParserConfigurationException, SAXException, IOException {
        fbsCore = new Core();
        slenListModel = new DefaultListModel();
        initComponents();
        initRequestList();
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of
     * this method is always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        seleniumFileChooser = new javax.swing.JFileChooser();
        addSelenTestFrame = new javax.swing.JFrame();
        jLabel4 = new javax.swing.JLabel();
        addTestName = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        addTestPath = new javax.swing.JTextField();
        addTestChooeFile = new javax.swing.JButton();
        saveStressTestButton = new javax.swing.JButton();
        editSelenTestFrame = new javax.swing.JFrame();
        jLabel6 = new javax.swing.JLabel();
        editTestName = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        editTestPath = new javax.swing.JTextField();
        editTestChooeFile = new javax.swing.JButton();
        editSaveStressTestButton = new javax.swing.JButton();
        addSelTest = new javax.swing.JButton();
        runAllTests = new javax.swing.JButton();
        runSingleTest = new javax.swing.JButton();
        slenLogs = new java.awt.TextArea();
        jLabel1 = new javax.swing.JLabel();
        returnMainMenu = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        slenTestList = new javax.swing.JList();
        removeSelenTest = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        editSelTest = new javax.swing.JButton();

        seleniumFileChooser.setDialogTitle("Choose .java File");
        seleniumFileChooser.setName("Select Selenium Java Test File");
        filter = new FileNameExtensionFilter("Java Test Files","java");
        seleniumFileChooser.setFileFilter(filter);

        addSelenTestFrame.setMinimumSize(new java.awt.Dimension(625, 157));

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel4.setText("Add FrontEnd Test");

        jLabel2.setText("Name :");

        jLabel5.setText("File Path:");

        addTestChooeFile.setText("Choose File");
        addTestChooeFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addTestChooeFileActionPerformed(evt);
            }
        });

        saveStressTestButton.setText("Save Test");
        saveStressTestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveStressTestButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout addSelenTestFrameLayout = new javax.swing.GroupLayout(addSelenTestFrame.getContentPane());
        addSelenTestFrame.getContentPane().setLayout(addSelenTestFrameLayout);
        addSelenTestFrameLayout.setHorizontalGroup(
            addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                        .addGroup(addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(addTestPath, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(addTestChooeFile, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(addTestName)))
                .addGap(23, 23, 23))
            .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                .addGap(254, 254, 254)
                .addComponent(saveStressTestButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addSelenTestFrameLayout.setVerticalGroup(
            addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addSelenTestFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addTestName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(addTestPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addTestChooeFile))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(saveStressTestButton)
                .addContainerGap())
        );

        editSelenTestFrame.setMinimumSize(new java.awt.Dimension(625, 157));

        jLabel6.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel6.setText("Add Selenium Test");

        jLabel7.setText("Name :");

        jLabel8.setText("File Path:");

        editTestChooeFile.setText("Choose File");
        editTestChooeFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editTestChooeFileActionPerformed(evt);
            }
        });

        editSaveStressTestButton.setText("Save Test");
        editSaveStressTestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSaveStressTestButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout editSelenTestFrameLayout = new javax.swing.GroupLayout(editSelenTestFrame.getContentPane());
        editSelenTestFrame.getContentPane().setLayout(editSelenTestFrameLayout);
        editSelenTestFrameLayout.setHorizontalGroup(
            editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                        .addGroup(editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editTestPath, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(editTestChooeFile, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(editTestName)))
                .addGap(23, 23, 23))
            .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                .addGap(254, 254, 254)
                .addComponent(editSaveStressTestButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        editSelenTestFrameLayout.setVerticalGroup(
            editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editSelenTestFrameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(editTestName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(editSelenTestFrameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(editTestPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(editTestChooeFile))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(editSaveStressTestButton)
                .addContainerGap())
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(824, 502));

        addSelTest.setText("Add Test");
        addSelTest.setToolTipText("");
        addSelTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSelTestActionPerformed(evt);
            }
        });

        runAllTests.setText("Run All");
        runAllTests.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runAllTestsActionPerformed(evt);
            }
        });

        runSingleTest.setText("Run Selected");
        runSingleTest.setToolTipText("");
        runSingleTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                runSingleTestActionPerformed(evt);
            }
        });

        jLabel1.setText("Logs");

        returnMainMenu.setText("< back");
        returnMainMenu.setToolTipText("");
        returnMainMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                returnMainMenuActionPerformed(evt);
            }
        });

        slenTestList.setModel(slenListModel);
        jScrollPane1.setViewportView(slenTestList);

        removeSelenTest.setText("Remove Test");
        removeSelenTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeSelenTestActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        jLabel3.setText("Front-end Test Interface");

        editSelTest.setText("Edit Test");
        editSelTest.setToolTipText("");
        editSelTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSelTestActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(runAllTests)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(runSingleTest))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(slenLogs, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(addSelTest)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(editSelTest)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(removeSelenTest, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 418, Short.MAX_VALUE)
                        .addComponent(returnMainMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(returnMainMenu)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(runAllTests)
                            .addComponent(runSingleTest))
                        .addComponent(removeSelenTest, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(addSelTest)
                        .addComponent(editSelTest)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(1, 1, 1)
                .addComponent(slenLogs, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void returnMainMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_returnMainMenuActionPerformed
        try {
            setVisible(false);
            dispose();
            MainPage newSelPage = new MainPage();
            newSelPage.setVisible(true);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_returnMainMenuActionPerformed

    private void addSelTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSelTestActionPerformed
        addSelenTestFrame.setVisible(true);
    }//GEN-LAST:event_addSelTestActionPerformed

    private void removeSelenTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeSelenTestActionPerformed

        if (slenTestList.getSelectedValuesList().isEmpty()) {
            System.err.println("You did not select element");
        } else {
            Object[] options = {"Yes", "No"};
            int result = JOptionPane.showOptionDialog(getContentPane(), "Are you sure ?", "Delete Test", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (result == 0) {
                slenListModel.removeElement(slenTestList.getSelectedValue());
            }
        }
    }//GEN-LAST:event_removeSelenTestActionPerformed

    private void runSingleTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runSingleTestActionPerformed

        int selIndex= slenTestList.getSelectedIndex();
        try {
            slenLogs.append(fbsCore.runSingleSeleniumTest(selIndex));
        } catch (IOException ex) {
            Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
        }

    }//GEN-LAST:event_runSingleTestActionPerformed

    private void runAllTestsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_runAllTestsActionPerformed
        try {
            fbsCore.runAllSeleniumTests(slenListModel);
        } catch (IOException ex) {
            Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_runAllTestsActionPerformed

    private void editSelTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSelTestActionPerformed
        SelectedIndex = slenTestList.getSelectedIndex();
        editTestName.setText(fbsCore.getTestNameByIndex(SelectedIndex));
        editTestPath.setText(fbsCore.getTestFilePathByIndex(SelectedIndex));
        editSelenTestFrame.setVisible(true);
    }//GEN-LAST:event_editSelTestActionPerformed

    private void addTestChooeFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addTestChooeFileActionPerformed

        //Return value to us successful message or unsuccesfull message
        int returnValue = seleniumFileChooser.showSaveDialog(addSelenTestFrame);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = seleniumFileChooser.getSelectedFile();
            addTestPath.setText(file.getPath());

        } else {
            System.err.println("File Can Not Select !");
        }
    }//GEN-LAST:event_addTestChooeFileActionPerformed

    private void saveStressTestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveStressTestButtonActionPerformed
        try {
            fbsCore.addStressTestToXml(addTestName.getText(), addTestPath.getText());
            addTestName.setText("");
            addTestPath.setText("");
            addSelenTestFrame.setVisible(false);
            refreshList();
        } catch (TransformerException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_saveStressTestButtonActionPerformed

    private void editTestChooeFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editTestChooeFileActionPerformed
        int returnValue = seleniumFileChooser.showSaveDialog(editSelenTestFrame);

        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File file = seleniumFileChooser.getSelectedFile();
            editTestPath.setText(file.getPath());
        } else {
            System.err.println("File Can Not Select !");
        }
    }//GEN-LAST:event_editTestChooeFileActionPerformed

    private void editSaveStressTestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSaveStressTestButtonActionPerformed
        try {
            fbsCore.editStressTest(SelectedIndex, editTestName.getText(), editTestPath.getText());
            editSelenTestFrame.setVisible(false);
            refreshList();
        } catch (TransformerException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(StressTestPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_editSaveStressTestButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SeleniumTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SeleniumTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SeleniumTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SeleniumTestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new SeleniumTestPage().setVisible(true);
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(SeleniumTestPage.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addSelTest;
    private javax.swing.JFrame addSelenTestFrame;
    private javax.swing.JButton addTestChooeFile;
    private javax.swing.JTextField addTestName;
    private javax.swing.JTextField addTestPath;
    private javax.swing.JButton editSaveStressTestButton;
    private javax.swing.JButton editSelTest;
    private javax.swing.JFrame editSelenTestFrame;
    private javax.swing.JButton editTestChooeFile;
    private javax.swing.JTextField editTestName;
    private javax.swing.JTextField editTestPath;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton removeSelenTest;
    private javax.swing.JButton returnMainMenu;
    private javax.swing.JButton runAllTests;
    private javax.swing.JButton runSingleTest;
    private javax.swing.JButton saveStressTestButton;
    private javax.swing.JFileChooser seleniumFileChooser;
    private java.awt.TextArea slenLogs;
    private javax.swing.JList slenTestList;
    // End of variables declaration//GEN-END:variables
    private DefaultListModel slenListModel;
    private FileNameExtensionFilter filter;
    private int SelectedIndex;
   private void initRequestList() throws ParserConfigurationException, IOException {
        List<String> listArray = fbsCore.getListofRequests();
        for (int i = 0; i < listArray.size(); i++) {
            slenListModel.addElement(listArray.get(i));
        }
    }

    private void refreshList() throws ParserConfigurationException, IOException {
        slenListModel.clear();
        initRequestList();
    }
}
